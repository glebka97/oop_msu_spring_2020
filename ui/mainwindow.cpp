#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "dialog.h"

#include <QTableWidget>
#include <QDialog>

#include <iostream>
#include <memory>

namespace {

  void addConfiguredTableWidget(QTableWidget* table_ptr, const QStringList& header) {
    table_ptr->setRowCount(15);
    table_ptr->setColumnCount(header.size());
    table_ptr->setHorizontalHeaderLabels(header);
    table_ptr->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table_ptr->setSelectionBehavior(QAbstractItemView::SelectRows);
    table_ptr->setSelectionMode(QAbstractItemView::SingleSelection);
    table_ptr->setShowGrid(false);
    table_ptr->setStyleSheet("QTableView {selection-background-color: grey;}");
  }

}

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  Config config;
  Dialog configuration_dialog_window;
  int result = configuration_dialog_window.exec();
  if (result == QDialog::Accepted) {
    config = configuration_dialog_window.getConfig();
  } else if (result == QDialog::Rejected) {
    std::cerr << "Using default configurations" << std::endl;
  } else {
    throw std::logic_error("wrong dialog behavior");
  }

  simulation_.reset(new Simulation(config));

  ui->setupUi(this);

  QStringList warehouseHeader;
  warehouseHeader << "Название" << "Автор" << "Год публикации" << "Год издания" << "Издательство"
      << "Цена" << "Наценка" << "Кол-во";
  addConfiguredTableWidget(ui->warehouseTable, warehouseHeader);

  QStringList completedOrdersHeader;
  completedOrdersHeader << "Фамилия" << "Имя" << "Телефон" << "Почта" << "Название" << "Автор"
      << "Кол-во" << "Дата";
  addConfiguredTableWidget(ui->completedOrdersTable, completedOrdersHeader);

  QStringList newOrdersHeader;
  newOrdersHeader << "Фамилия" << "Имя" << "Телефон" << "Почта" << "Название" << "Автор"
      << "Кол-во" << "Дата";
  addConfiguredTableWidget(ui->newOrdersTable, newOrdersHeader);

  QStringList publishersHeader;
  publishersHeader << "Номер заявки" << "Статус заявки" << "Название" << "Автор" << "Издательство" << "Год издания";
  addConfiguredTableWidget(ui->publishersTable, publishersHeader);

  updateAllTables();
  updateStatistics();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::updateAllTables() {
  updateWarehouseTableData();
  updateCompletedOrdersTableData();
  updateNewOrdersTableData();
  updatePublishersTableData();
}

void MainWindow::updateWarehouseTableData() {
  int i = 0;
  for (const auto& [book_info, book_in_store] : simulation_->getStoreWarehouse()) {
    ui->warehouseTable->setItem(i, 0, new QTableWidgetItem(QString::fromUtf8(book_info.title.c_str())));
    ui->warehouseTable->setItem(i, 1, new QTableWidgetItem(QString::fromUtf8(book_info.author.c_str())));
    ui->warehouseTable->setItem(i, 2, new QTableWidgetItem(QString::fromUtf8(std::to_string(book_info.publish_year).c_str())));
    ui->warehouseTable->setItem(i, 3, new QTableWidgetItem(QString::fromUtf8(std::to_string(book_info.year).c_str())));
    ui->warehouseTable->setItem(i, 4, new QTableWidgetItem(QString::fromUtf8(book_info.publisher.c_str())));
    ui->warehouseTable->setItem(i, 5, new QTableWidgetItem(QString::fromUtf8(std::to_string(book_in_store->getCost()).c_str())));
    ui->warehouseTable->setItem(i, 6, new QTableWidgetItem(QString::fromUtf8(std::to_string(book_in_store->getAdditionalCost()).c_str())));
    ui->warehouseTable->setItem(i, 7, new QTableWidgetItem(QString::fromUtf8(std::to_string(book_in_store->getCount()).c_str())));
    ++i;
  }
  ui->warehouseTable->resizeColumnsToContents();
}

void MainWindow::updateCompletedOrdersTableData() {
  int i = 0;
  ui->completedOrdersTable->setRowCount(0);
  for (const auto& order : simulation_->getProcessedOrders()) {
    for (const auto& book : order.books_in_order) {
      ui->completedOrdersTable->setRowCount(i + 1);
      ui->completedOrdersTable->setItem(i, 0, new QTableWidgetItem(QString::fromUtf8(order.customer_info.last_name.c_str())));
      ui->completedOrdersTable->setItem(i, 1, new QTableWidgetItem(QString::fromUtf8(order.customer_info.first_name.c_str())));
      ui->completedOrdersTable->setItem(i, 2, new QTableWidgetItem(QString::fromUtf8(order.customer_info.phone_number.c_str())));
      ui->completedOrdersTable->setItem(i, 3, new QTableWidgetItem(QString::fromUtf8(order.customer_info.email_address.c_str())));
      ui->completedOrdersTable->setItem(i, 4, new QTableWidgetItem(QString::fromUtf8(book.book_info.title.c_str())));
      ui->completedOrdersTable->setItem(i, 5, new QTableWidgetItem(QString::fromUtf8(book.book_info.author.c_str())));
      ui->completedOrdersTable->setItem(i, 6, new QTableWidgetItem(QString::fromUtf8(std::to_string(book.count).c_str())));
      ui->completedOrdersTable->setItem(i, 7, new QTableWidgetItem(QString::fromUtf8(std::to_string(order.day_id).c_str())));
      ++i;
    }
  }
  ui->completedOrdersTable->resizeColumnsToContents();
}

void MainWindow::updateNewOrdersTableData() {
  int i = 0;
  ui->newOrdersTable->setRowCount(0);
  for (const auto& order : simulation_->getNewOrders()) {
    for (const auto& book : order.books_in_order) {
      ui->newOrdersTable->setRowCount(i + 1);
      ui->newOrdersTable->setItem(i, 0, new QTableWidgetItem(QString::fromUtf8(order.customer_info.last_name.c_str())));
      ui->newOrdersTable->setItem(i, 1, new QTableWidgetItem(QString::fromUtf8(order.customer_info.first_name.c_str())));
      ui->newOrdersTable->setItem(i, 2, new QTableWidgetItem(QString::fromUtf8(order.customer_info.phone_number.c_str())));
      ui->newOrdersTable->setItem(i, 3, new QTableWidgetItem(QString::fromUtf8(order.customer_info.email_address.c_str())));
      ui->newOrdersTable->setItem(i, 4, new QTableWidgetItem(QString::fromUtf8(book.book_info.title.c_str())));
      ui->newOrdersTable->setItem(i, 5, new QTableWidgetItem(QString::fromUtf8(book.book_info.author.c_str())));
      ui->newOrdersTable->setItem(i, 6, new QTableWidgetItem(QString::fromUtf8(std::to_string(book.count).c_str())));
      ui->newOrdersTable->setItem(i, 7, new QTableWidgetItem(QString::fromUtf8(std::to_string(order.day_id).c_str())));
      ++i;
    }
  }
  ui->newOrdersTable->resizeColumnsToContents();
}

void MainWindow::updatePublishersTableData() {
  int i = 0;
  ui->publishersTable->setRowCount(0);
  for (const auto& publisher : simulation_->getPublishersList()) {
    for (const auto& [_, request] : simulation_->getRequestsByPublisher(publisher)) {
      for (const auto& book : request.books) {
        const auto& book_info = book.book_info;
        ui->publishersTable->setRowCount(i + 1);
        ui->publishersTable->setItem(i, 0, new QTableWidgetItem(QString::fromUtf8(std::to_string(request.id).c_str())));
        ui->publishersTable->setItem(i, 1, new QTableWidgetItem(QString::fromUtf8(request.is_completed ? "выполнено" : "не выполнено")));
        ui->publishersTable->setItem(i, 2, new QTableWidgetItem(QString::fromUtf8(book_info.title.c_str())));
        ui->publishersTable->setItem(i, 3, new QTableWidgetItem(QString::fromUtf8(book_info.author.c_str())));
        ui->publishersTable->setItem(i, 4, new QTableWidgetItem(QString::fromUtf8(book_info.publisher.c_str())));
        ui->publishersTable->setItem(i, 5, new QTableWidgetItem(QString::fromUtf8(std::to_string(book_info.publish_year).c_str())));
        ++i;
      }
    }
  }
  ui->publishersTable->resizeColumnsToContents();
}

void MainWindow::updateStatistics() {
  const auto& stats = simulation_->getSimulationStatistics();
  ui->lineEdit0->setPlaceholderText(QString::fromUtf8(std::to_string(stats.day_id).c_str()));
  ui->lineEdit1->setPlaceholderText(QString::fromUtf8(std::to_string(stats.profit).c_str()));
  ui->lineEdit2->setPlaceholderText(
      QString::fromUtf8(std::to_string(stats.books_sold).c_str())
  );
  ui->lineEdit3->setPlaceholderText(
      QString::fromUtf8(std::to_string(stats.new_orders_count).c_str())
  );
  ui->lineEdit4->setPlaceholderText(
      QString::fromUtf8(std::to_string(stats.processed_orders_count).c_str())
  );
  ui->lineEdit5->setPlaceholderText(
      QString::fromUtf8(std::to_string(stats.completed_requests_count).c_str())
  );
}

void MainWindow::on_pushButton_clicked() {
  simulation_->processDay();
  updateAllTables();
  updateStatistics();
}

void MainWindow::on_pushButton_2_clicked() {
  simulation_->finishSimulation();
  updateAllTables();
  updateStatistics();
}
