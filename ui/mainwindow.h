#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../simulation/simulation.h"

#include <QMainWindow>

#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_pushButton_clicked();

  void on_pushButton_2_clicked();

private:
  void updateAllTables();
  void updateWarehouseTableData();
  void updateCompletedOrdersTableData();
  void updateNewOrdersTableData();
  void updatePublishersTableData();
  void updateStatistics();

private:
  Ui::MainWindow *ui;
  std::unique_ptr<Simulation> simulation_;
};
#endif // MAINWINDOW_H
