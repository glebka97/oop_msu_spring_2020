#include "dialog.h"
#include "ui_dialog.h"

#include <cstdlib>

Dialog::Dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Dialog)
{
  ui->setupUi(this);
}

Dialog::~Dialog()
{
  delete ui;
}

Config Dialog::getConfig() {
  Config config = {
    std::atoi(ui->lineEdit0->text().toUtf8().constData()),
    std::atoi(ui->lineEdit1->text().toUtf8().constData()),
    std::atoi(ui->lineEdit2->text().toUtf8().constData()),
    std::atoi(ui->lineEdit3->text().toUtf8().constData()),
    std::atoi(ui->lineEdit4->text().toUtf8().constData()),
    std::atoi(ui->lineEdit5->text().toUtf8().constData()),
    std::atoi(ui->lineEdit6->text().toUtf8().constData()),
    std::atoi(ui->lineEdit7->text().toUtf8().constData()),
    std::atoi(ui->lineEdit8->text().toUtf8().constData()),
    std::atoi(ui->lineEdit9->text().toUtf8().constData()),
    std::atoi(ui->lineEdit10->text().toUtf8().constData()),
    std::atoi(ui->lineEdit11->text().toUtf8().constData()),
  };
  return config;
}

