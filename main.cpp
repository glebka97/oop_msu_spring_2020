#include "ui/mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    MainWindow main_window;
    main_window.setWindowTitle("Book Store Simulation");
    main_window.showMaximized();
    return application.exec();
}
