#include "publisher.h"

#include <algorithm>

Publisher::Publisher(
    const Config& config,
    std::shared_ptr<std::mt19937> random_generator
) : random_generator_(random_generator),
    exec_time_dist_(1, config.request_exec_time)
{}

const std::map<RequestId, Request>& Publisher::getRequests() const {
  return request_with_id_;
}

void Publisher::acquireRequests(const Request& request, int day_id) {
  request_with_id_[request.id] = request;
  requests_[day_id + exec_time_dist_(*random_generator_)].push_back(request.id);
}

void Publisher::getCompletedRequests(std::vector<RequestId>& requests, int day_id) {
  auto& requests_today = requests_[day_id];
  std::copy(
      requests_today.begin(),
      requests_today.end(),
      std::back_inserter(requests)
  );
  for (auto& id : requests_today) {
    request_with_id_[id].is_completed = true;
  }
}
