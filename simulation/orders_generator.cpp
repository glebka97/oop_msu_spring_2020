#include "orders_generator.h"

#include "order.h"

#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <string>

OrdersGenerator::OrdersGenerator(
    const std::filesystem::path& books_file_path,
    const std::filesystem::path& customers_file_path,
    const OrdersGeneratorConfiguration& configuration,
    int random_generator_seed
) : configuration_(configuration), random_generator_(random_generator_seed)
{
  std::ifstream books_stream(books_file_path);
  for(std::string line; std::getline(books_stream, line);) {
    books_.push_back(parseBookInOrder(line));
  }
  if (books_.size() < static_cast<size_t>(configuration_.books_count_upper_bound)) {
    throw std::out_of_range("configuration_.books_count_upper_bound is too big");
  }

  std::ifstream customers_stream(customers_file_path);
  for(std::string line; std::getline(customers_stream, line);) {
    customers_.push_back(parseCustomerInfo(line));
  }
}

std::vector<Order> OrdersGenerator::generateNewOrders(int day_id) {
  std::uniform_int_distribution<> orders_count_distribution(
      configuration_.orders_count_lower_bound,
      configuration_.orders_count_upper_bound
  );
  size_t new_orders_count = orders_count_distribution(random_generator_);
  std::vector<Order> new_orders(new_orders_count);

  std::uniform_int_distribution<> books_distribution(
      configuration_.books_count_lower_bound,
      configuration_.books_count_upper_bound
  );
  std::uniform_int_distribution<> book_distribution(
      configuration_.book_count_lower_bound,
      configuration_.book_count_upper_bound
  );
  std::uniform_int_distribution<> customers_distribution(0, customers_.size() - 1);
  for(auto& order : new_orders) {
    order.day_id = day_id;
    size_t customer_id = customers_distribution(random_generator_);
    order.customer_info = customers_[customer_id];

    size_t books_count = books_distribution(random_generator_);
    order.books_in_order.resize(books_count);
    std::sample(
        books_.begin(),
        books_.end(),
        order.books_in_order.begin(),
        books_count,
        random_generator_
    );
    for (auto& book : order.books_in_order) {
      book.count = book_distribution(random_generator_);
    }
  }

  return new_orders;
}
