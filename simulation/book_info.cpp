#include "book_info.h"

#include <tuple>

namespace {

  int stringToInt(const std::string& string_value) {
    std::istringstream string_stream(string_value);
    int int_value;
    string_stream >> int_value;
    return int_value;
  }

}

int readIntFieldFromStream(std::istringstream& int_field_stream) {
  std::string int_field_string;
  constexpr char delimeter = '\t';
  std::getline(int_field_stream, int_field_string, delimeter);
  return stringToInt(int_field_string);
}

BookInfo parseBookInfo(std::istringstream& book_stream) {
  BookInfo book_info;
  constexpr char delimeter = '\t';
  std::getline(book_stream, book_info.title, delimeter);
  std::getline(book_stream, book_info.author, delimeter);
  book_info.publish_year = readIntFieldFromStream(book_stream);
  std::getline(book_stream, book_info.publisher, delimeter);
  book_info.year = readIntFieldFromStream(book_stream);

  return book_info;
}

bool operator<(const BookInfo& lhs, const BookInfo& rhs) {
  return std::tie(
      lhs.title,
      lhs.author,
      lhs.publish_year,
      lhs.publisher,
      lhs.year
  ) < std::tie(
      rhs.title,
      rhs.author,
      rhs.publish_year,
      rhs.publisher,
      rhs.year
  );
}

