#pragma once

#include "order.h"

#include <filesystem>
#include <random>
#include <vector>

struct OrdersGeneratorConfiguration {
  int orders_count_lower_bound;
  int orders_count_upper_bound;
  int books_count_lower_bound;
  int books_count_upper_bound;
  int book_count_lower_bound;
  int book_count_upper_bound;
};

class OrdersGenerator {
public:
  OrdersGenerator(
      const std::filesystem::path& books_file_path,
      const std::filesystem::path& customers_file_path,
      const OrdersGeneratorConfiguration& configuration,
      int random_generator_seed = 2312
  );

  std::vector<Order> generateNewOrders(int day_id);

private:
  std::vector<BookInOrder> books_;
  std::vector<CustomerInfo> customers_;
  OrdersGeneratorConfiguration configuration_;
  std::mt19937 random_generator_;
};
