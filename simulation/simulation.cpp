#include "simulation.h"

namespace {
  const std::string BOOKS_DATA_PATH =
    "/home/gsavchenko/repos/oop_msu_spring_2020/data/books.txt";
  const std::string CUSTOMERS_DATA_PATH =
    "/home/gsavchenko/repos/oop_msu_spring_2020/data/customers.txt";
}

Simulation::Simulation(const Config& config)
: last_day_(config.days_count),
  store_(std::make_unique<Store>(BOOKS_DATA_PATH, config)),
  publishers_handler_(std::make_unique<PublishersHandler>(config)),
  orders_generator_(std::make_unique<OrdersGenerator>(
        BOOKS_DATA_PATH,
        CUSTOMERS_DATA_PATH,
        config.og_config
  ))
{}

const std::map<BookInfo, std::shared_ptr<BookInStore>>& Simulation::getStoreWarehouse() const {
  return store_->getWarehouse();
}

const std::list<Order>& Simulation::getNewOrders() const {
  return store_->getNewOrders();
}

const std::vector<Order>& Simulation::getProcessedOrders() const {
  return store_->getProcessedOrders();
}

const std::map<RequestId, Request>& Simulation::getRequestsByPublisher(
    const std::string& publisher_name
) const {
  return publishers_handler_->getRequestsByPublisher(publisher_name);
}

const std::vector<std::string>& Simulation::getPublishersList() const {
  return publishers_handler_->getPublishersList();
}

const SimulationStatistics& Simulation::getSimulationStatistics() const {
  return stats_;
}

void Simulation::processDay() {
  if (stats_.day_id > last_day_) return;
  ++stats_.day_id;

  auto completed_requests =
    publishers_handler_->getCompletedRequests(stats_.day_id);
  stats_.completed_requests_count += completed_requests.size();
  store_->acquireCompletedRequests(completed_requests);

  auto new_orders = orders_generator_->generateNewOrders(stats_.day_id);
  store_->acquireNewOrders(new_orders);

  auto [profit, books_count] = store_->processOrders();
  stats_.profit += profit;
  stats_.books_sold += books_count;

  auto requests = store_->createRequests(stats_.day_id);
  publishers_handler_->acquireRequests(requests, stats_.day_id);

  stats_.new_orders_count = getNewOrders().size();
  stats_.processed_orders_count = getProcessedOrders().size();
}

void Simulation::finishSimulation() {
  while (stats_.day_id <= last_day_) {
    processDay();
  }
}

