#include "warehouse.h"

#include <fstream>

Warehouse::Warehouse(
    const std::filesystem::path& books_file_path,
    const Config& config,
    int seed
) : book_to_request_threshold_(config.warehouse_threshold),
  new_request_count_(config.new_request_books_count),
  random_generator_(seed),
  books_count_dist_(
      config.warehouse_books_count_min,
      config.warehouse_books_count_max
  )
{
  std::ifstream books_stream(books_file_path);
  for (std::string line; std::getline(books_stream, line);) {
    auto book = std::make_shared<BookInStore>(line, books_count_dist_, random_generator_);
    books_[book->getBookInfo()] = book;
  }
}

const std::map<BookInfo, std::shared_ptr<BookInStore>>& Warehouse::getBooks() const {
  return books_;
}

void Warehouse::addNewBooks(const std::vector<Request>& requests) {
  for (const auto& request : requests) {
    for (const auto& book : request.books) {
      books_[book.book_info]->getCount() += book.count;
      auto& book_request_id = books_[book.book_info]->getUncompletedBasicRequestId();
      if (book_request_id &&
          *book_request_id == request.id) {
        book_request_id = std::nullopt;
      }
    }
  }
}

bool Warehouse::processBookInOrder(BookInOrder& book) {
  const BookInfo& book_info = book.book_info;
  auto book_in_store = books_[book_info];
  int wanted_count = book.count - book.completed_count;
  if (wanted_count == 0) {
    return true;
  }
  int& warehouse_count = book_in_store->getCount();
  int to_get = std::min(wanted_count, warehouse_count);
  book.completed_count += to_get;
  warehouse_count -= to_get;

  return book.count - book.completed_count == 0;
}

int Warehouse::getAdditionalCost(const BookInfo& book) const {
  return books_.at(book)->getAdditionalCost();
}

std::unordered_map<std::string, Request> Warehouse::createRequests(
    const std::list<Order>& orders,
    int day_id
) {
  std::unordered_map<std::string, Request> requests;
  for (const auto& order : orders) {
    if (order.day_id != day_id) {
      continue;
    }
    for (const auto& book : order.books_in_order) {
      int wanted_count = book.count - book.completed_count;
      if (wanted_count == 0) {
        continue;
      }
      setRequestId(requests, book.book_info.publisher);
      requests[book.book_info.publisher].books.push_back(
          {book.book_info, wanted_count}
      );
    }
  }

  for (auto& [book_info, book_in_store] : books_) {
    if (book_in_store->getCount() < book_to_request_threshold_ &&
        !book_in_store->getUncompletedBasicRequestId()) {
      const auto& publisher = book_info.publisher;
      setRequestId(requests, publisher);
      book_in_store->getUncompletedBasicRequestId() = requests[publisher].id;
      requests[publisher].books.push_back({book_info, new_request_count_});
    }
  }

  return requests;
}

void Warehouse::setRequestId(
    std::unordered_map<std::string, Request>& requests,
    const std::string& publisher
) {
  if (requests.find(publisher) == requests.end()) {
    requests[publisher].id = next_id_;
    ++next_id_;
  }
}
