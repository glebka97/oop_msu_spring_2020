#include "order.h"

#include <sstream>

BookInOrder parseBookInOrder(const std::string& book_raw) {
  BookInOrder book_in_order;
  std::istringstream book_in_order_raw(book_raw);
  book_in_order.book_info = parseBookInfo(book_in_order_raw);

  return book_in_order;
}

CustomerInfo parseCustomerInfo(const std::string& customer_raw) {
  CustomerInfo customer_info;
  constexpr char delimeter = '\t';
  std::istringstream customer_info_raw(customer_raw);
  std::getline(customer_info_raw, customer_info.last_name, delimeter);
  std::getline(customer_info_raw, customer_info.first_name, delimeter);
  std::getline(customer_info_raw, customer_info.phone_number, delimeter);
  std::getline(customer_info_raw, customer_info.email_address, delimeter);

  return customer_info;
}
