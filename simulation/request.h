#pragma once

#include "book_info.h"

#include <vector>

struct BookInRequest {
  BookInfo book_info;
  int count;
};

using RequestId = int;

struct Request {
  RequestId id;
  bool is_completed = false;
  std::vector<BookInRequest> books;
};

