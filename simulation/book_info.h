#pragma once

#include <sstream>
#include <string>

int readIntFieldFromStream(std::istringstream& int_field_stream);

struct BookInfo {
  std::string title;
  std::string author;
  int publish_year;
  std::string publisher;
  int year;
};

BookInfo parseBookInfo(std::istringstream& book_stream);

bool operator<(const BookInfo& lhs, const BookInfo& rhs);

