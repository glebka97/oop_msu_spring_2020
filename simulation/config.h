#pragma once

#include "orders_generator.h"

struct Config {
  int days_count = 30;
  int warehouse_threshold = 3;
  int new_request_books_count = 20;
  int warehouse_books_count_min = 0;
  int warehouse_books_count_max = 20;
  int request_exec_time = 5;
  OrdersGeneratorConfiguration og_config = {
    3, 15,
    1, 3,
    1, 5
  };
};
