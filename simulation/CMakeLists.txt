cmake_minimum_required(VERSION 3.5)

add_library(simulation_lib STATIC
  book_in_store.cpp
  book_in_store.h
  book_info.cpp
  book_info.h
  config.cpp
  config.h
  publisher.cpp
  publisher.h
  publishers_handler.cpp
  publishers_handler.h
  simulation.cpp
  simulation.h
  store.cpp
  store.h
  order.cpp
  order.h
  warehouse.cpp
  warehouse.h
  orders_generator.cpp
  orders_generator.h
)

add_executable(test_orders_generator tests/test_orders_generator.cpp)
target_link_libraries(test_orders_generator simulation_lib)
