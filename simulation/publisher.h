#pragma once

#include "config.h"
#include "request.h"

#include <map>
#include <memory>
#include <random>
#include <unordered_map>
#include <vector>

class Publisher {
public:
  Publisher(
      const Config& config,
      std::shared_ptr<std::mt19937> random_generator
  );

  const std::map<RequestId, Request>& getRequests() const;

  void acquireRequests(const Request& request, int day_id);

  void getCompletedRequests(std::vector<RequestId>& requests, int day_id);

private:
  std::map<RequestId, Request> request_with_id_;
  std::unordered_map<int, std::vector<RequestId>> requests_;
  std::shared_ptr<std::mt19937> random_generator_;
  std::uniform_int_distribution<> exec_time_dist_;
};
