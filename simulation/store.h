#pragma once

#include "book_in_store.h"
#include "book_info.h"
#include "config.h"
#include "order.h"
#include "request.h"
#include "warehouse.h"

#include <filesystem>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

class Store {
public:
  Store(const std::filesystem::path& books_file_path, const Config& config);

  const std::map<BookInfo, std::shared_ptr<BookInStore>>& getWarehouse() const;

  const std::list<Order>& getNewOrders() const;

  const std::vector<Order>& getProcessedOrders() const;

  void acquireCompletedRequests(const std::vector<RequestId>& completed_requests);

  void acquireNewOrders(std::vector<Order>& new_orders);

  std::pair<int, int> processOrders();

  std::unordered_map<std::string, Request> createRequests(int day_id);

private:
  std::list<Order> new_orders_;
  std::vector<Order> processed_orders_;
  std::unordered_map<RequestId, Request> requests_;
  std::unique_ptr<Warehouse> warehouse_;
};
