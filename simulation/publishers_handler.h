#pragma once

#include "config.h"
#include "publisher.h"
#include "request.h"

#include <map>
#include <memory>
#include <random>
#include <string>
#include <unordered_map>
#include <vector>

class PublishersHandler {
public:
  PublishersHandler(const Config& config);

  std::vector<RequestId> getCompletedRequests(int day_id) const;

  const std::map<RequestId, Request>& getRequestsByPublisher(
      const std::string& publisher_name
  );

  const std::vector<std::string>& getPublishersList() const;

  void acquireRequests(const std::unordered_map<std::string, Request>& requests, int day_id);

private:
  std::shared_ptr<std::mt19937> random_generator_;
  const std::vector<std::string> publishers_names_ = {
    "Просвещение",
    "Эксмо — АСТ",
    "Азбука — Аттикус"
  };
  std::unordered_map<std::string, std::shared_ptr<Publisher>> publishers_;
};
