#pragma once

#include "book_in_store.h"
#include "book_info.h"
#include "config.h"
#include "order.h"
#include "request.h"

#include <filesystem>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

class Warehouse {
public:
  Warehouse(
      const std::filesystem::path& books_file_path,
      const Config& config,
      int seed = 2314
  );

  const std::map<BookInfo, std::shared_ptr<BookInStore>>& getBooks() const;

  void addNewBooks(const std::vector<Request>& requests);

  bool processBookInOrder(BookInOrder& book);

  int getAdditionalCost(const BookInfo& book) const;

  std::unordered_map<std::string, Request> createRequests(
      const std::list<Order>& orders,
      int day_id
  );

private:
  void setRequestId(
      std::unordered_map<std::string, Request>& requests,
      const std::string& publisher
  );

private:
  const int book_to_request_threshold_;
  const int new_request_count_;
  RequestId next_id_ = 0;
  std::mt19937 random_generator_;
  std::uniform_int_distribution<> books_count_dist_;
  std::map<BookInfo, std::shared_ptr<BookInStore>> books_;
};
