#include "../orders_generator.h"

#include <iostream>

int main() {
  OrdersGeneratorConfiguration conf = {1, 3, 1, 3, 1, 4};
  OrdersGenerator og("../data/books.txt", "../data/customers.txt", conf, 1234);
  auto v = og.generateNewOrders(1);
  for (auto& x : v) {
    std::cout << x.customer_info.last_name << " " << x.customer_info.email_address << " " << x.books_in_order.size() << "\n";
    for (auto& y : x.books_in_order) {
      std::cout << y.book_info.author << " " << y.book_info.title << " " << y.count << "\n";
    }
    std::cout << "\n";
  }
  return 0;
}
