#include "publishers_handler.h"

PublishersHandler::PublishersHandler(const Config& config)
  : random_generator_(std::make_shared<std::mt19937>(1234))
{
  for (const auto& publisher_name : publishers_names_) {
    publishers_[publisher_name] = std::make_shared<Publisher>(config, random_generator_);
  }
}

std::vector<RequestId> PublishersHandler::getCompletedRequests(int day_id) const {
  std::vector<RequestId> result;
  for (const auto& [publisher_name, publisher] : publishers_) {
    publisher->getCompletedRequests(result, day_id);
  }
  return result;
}

const std::map<RequestId, Request>& PublishersHandler::getRequestsByPublisher(
    const std::string& publisher_name
) {
  return publishers_[publisher_name]->getRequests();
}

const std::vector<std::string>& PublishersHandler::getPublishersList() const {
  return publishers_names_;
}

void PublishersHandler::acquireRequests(const std::unordered_map<std::string, Request>& requests, int day_id) {
  for (const auto& [publisher_name, request] : requests) {
    publishers_[publisher_name]->acquireRequests(request, day_id);
  }
}
