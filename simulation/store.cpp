#include "store.h"

#include <algorithm>

Store::Store(const std::filesystem::path& books_file_path, const Config& config)
  : warehouse_(std::make_unique<Warehouse>(
        books_file_path,
        config
    ))
{}

const std::map<BookInfo, std::shared_ptr<BookInStore>>& Store::getWarehouse() const {
  return warehouse_->getBooks();
}

const std::list<Order>& Store::getNewOrders() const {
  return new_orders_;
}

const std::vector<Order>& Store::getProcessedOrders() const {
  return processed_orders_;
}

void Store::acquireCompletedRequests(
    const std::vector<RequestId>& completed_requests
) {
  std::vector<Request> requests;
  for (const auto& request_id : completed_requests) {
    requests.push_back(requests_.at(request_id));
  }
  warehouse_->addNewBooks(requests);
}

void Store::acquireNewOrders(std::vector<Order>& new_orders) {
  std::copy(new_orders.begin(), new_orders.end(), std::back_inserter(new_orders_));
}

std::pair<int, int> Store::processOrders() {
  std::vector<std::list<Order>::iterator> for_erase;
  int profit = 0;
  int books_count = 0;
  for (auto it = new_orders_.begin(); it != new_orders_.end(); ++it) {
    auto& order = *it;
    bool completed_order = true;
    for (auto& book : order.books_in_order) {
      completed_order &= warehouse_->processBookInOrder(book);

    }
    if (completed_order) {
      for (auto& book : order.books_in_order) {
        books_count += book.count;
        profit += book.count * warehouse_->getAdditionalCost(book.book_info);
      }
      for_erase.push_back(it);
    }
  }

  for (auto& it : for_erase) {
    processed_orders_.push_back(*it);
    new_orders_.erase(it);
  }
  return std::make_pair(profit, books_count);
}

std::unordered_map<std::string, Request> Store::createRequests(int day_id) {
  auto today_requests = warehouse_->createRequests(new_orders_, day_id);
  for (const auto& [_, request] : today_requests) {
    requests_[request.id] = request;
  }
  return today_requests;
}
