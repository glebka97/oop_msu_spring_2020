#pragma once

#include "book_info.h"

#include <string>
#include <vector>

struct BookInOrder {
  BookInfo book_info;
  int count = 0;
  int completed_count = 0;
};

BookInOrder parseBookInOrder(const std::string& book_raw);

struct CustomerInfo {
  std::string first_name;
  std::string last_name;
  std::string phone_number;
  std::string email_address;
};

CustomerInfo parseCustomerInfo(const std::string& customer_raw);

struct Order {
  CustomerInfo customer_info;
  std::vector<BookInOrder> books_in_order;
  int day_id;
};
