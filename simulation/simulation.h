#pragma once

#include "book_in_store.h"
#include "book_info.h"
#include "config.h"
#include "order.h"
#include "orders_generator.h"
#include "publishers_handler.h"
#include "request.h"
#include "store.h"

#include <filesystem>
#include <list>
#include <map>
#include <memory>
#include <vector>

struct SimulationStatistics {
  int day_id = 0;
  int profit = 0;
  int books_sold = 0;
  int new_orders_count = 0;
  int processed_orders_count = 0;
  int completed_requests_count = 0;
};

class Simulation {
public:
  Simulation(const Config& config);

  // Methods connected to UI
  const std::map<BookInfo, std::shared_ptr<BookInStore>>& getStoreWarehouse() const;

  const std::list<Order>& getNewOrders() const;

  const std::vector<Order>& getProcessedOrders() const;

  const std::map<RequestId, Request>& getRequestsByPublisher(
      const std::string& publisher_name
  ) const;

  const std::vector<std::string>& getPublishersList() const;

  const SimulationStatistics& getSimulationStatistics() const;

  // Methods connected to main logic
  void processDay();

  void finishSimulation();

private:
  const int last_day_;
  SimulationStatistics stats_;
  std::unique_ptr<Store> store_;
  std::unique_ptr<PublishersHandler> publishers_handler_;
  std::unique_ptr<OrdersGenerator> orders_generator_;
};
