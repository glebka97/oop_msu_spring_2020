#include "book_in_store.h"

#include <sstream>

BookInStore::BookInStore(
    const std::string& book_line,
    std::uniform_int_distribution<>& book_count_dist,
    std::mt19937& random_generator
) {
  std::istringstream book_line_stream(book_line);
  book_info_ = parseBookInfo(book_line_stream);
  cost_ = readIntFieldFromStream(book_line_stream);
  additional_cost_ = readIntFieldFromStream(book_line_stream);
  count_ = book_count_dist(random_generator);
}

int BookInStore::getCount() const {
  return count_;
}

int& BookInStore::getCount() {
  return count_;
}

const BookInfo& BookInStore::getBookInfo() const {
  return book_info_;
}

int BookInStore::getCost() const {
  return cost_;
}

int BookInStore::getAdditionalCost() const {
  return additional_cost_;
}

std::optional<RequestId>& BookInStore::getUncompletedBasicRequestId() {
  return uncompleted_basic_request_id_;
}
