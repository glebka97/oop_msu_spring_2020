#pragma once

#include "book_info.h"
#include "request.h"

#include <optional>
#include <string>
#include <random>

class BookInStore {
public:
  BookInStore(
      const std::string& book_line,
      std::uniform_int_distribution<>& book_count_dist,
      std::mt19937& random_generator
  );

  int getCount() const;

  int& getCount();

  const BookInfo& getBookInfo() const;

  int getCost() const;

  int getAdditionalCost() const;

  std::optional<RequestId>& getUncompletedBasicRequestId();

private:
  BookInfo book_info_;
  int cost_;
  int additional_cost_;
  int count_;
  std::optional<RequestId> uncompleted_basic_request_id_ = std::nullopt;
};

